# pylint: disable=missing-class-docstring, disable=missing-function-docstring, line-too-long
# pylint: disable=missing-module-docstring, too-many-instance-attributes, invalid-name, consider-using-f-string
# pylint: disable=too-few-public-methods, consider-using-in, chained-comparison, too-many-locals
# pylint: disable=no-else-raise, too-many-branches, too-many-statements

import logging
import sys

from isa import Opcode, OpType, read_code, TermTwoOperands, TermDestination, TermOneOperand

INSTRUCTION_START: int = 20
DATA_START: int = 0
MEM_SIZE: int = 50


class Memory:
    def __init__(self, mem_size: int, instruction_start: int, data_start: int):
        self.memory = [0] * mem_size
        self.instructions_start = instruction_start
        self.data_start = data_start

    def __getitem__(self, address: int):
        return self.memory[address]

    def __setitem__(self, address: int, value: int):
        self.memory[address] = value

    def load(self, type_of_memory: str, machine_code):
        type_map = {
            "instructions": (self.instructions_start, len(self.memory)),
            "data": (self.data_start, len(self.memory))
        }
        start, end = type_map[type_of_memory]
        if start + sum(1 if not isinstance(value, list) else len(value) for value in machine_code) > end:
            raise ValueError("Memory overflow")
        value = [
            val
            for sublist in machine_code
            for val in (sublist if isinstance(sublist, list) else [sublist])
        ]
        self.memory[start: start + len(value)] = value


class RegisterController:
    def __init__(self):
        self.PC: int = INSTRUCTION_START
        self.AR: int = 0
        self.DR: int = 0
        self.CIR: int = 0
        self.AC: int = 0
        self.registers: dict = {"RAX": 0, "RBX": 0, "RCX": 0, "RDX": 0}

    def fetch_instruction(self, memory):
        self.AR = self.PC
        self.DR = memory[self.AR]
        self.CIR = self.DR


class Alu:
    def __init__(self):
        self.left: int = 0
        self.right: int = 0
        self.operations: dict = {
            Opcode.DIV: lambda left, right: right / left,
            Opcode.MOD: lambda left, right: right % left,
            Opcode.CMP: lambda left, right: right - left,
            Opcode.ADD: lambda left, right: left + right,
            Opcode.SUB: lambda left, right: right - left,
            Opcode.MUL: lambda left, right: left * right,
        }
        self.flags: dict = {"N": False, "Z": False, "V": False, "C": False}


class DataPath:
    def __init__(self, register_controller: RegisterController, memory: Memory, alu: Alu, input_buffer: list):
        self.register_controller = register_controller
        self.memory = memory
        self.alu: Alu = alu
        self.output_buffer = []
        self.input_buffer = input_buffer

    def output(self):
        val = self.register_controller.registers[self.register_controller.DR]
        if 0 <= val <= 0x10FFFF:
            symbol = chr(val)
            if not symbol.isascii() and val != 32:
                symbol = str(val)
        else:
            symbol = str(val)
        logging.debug("output: %s << %s", repr("".join(self.output_buffer)), repr(symbol))
        self.output_buffer.append(symbol)

    def wr(self, op_type: OpType):
        if len(self.input_buffer) == 0:
            raise EOFError()
        symbol = self.input_buffer.pop(0)
        symbol_code = ord(symbol)
        assert -128 <= symbol_code <= 127, "input token is out of bound: {}".format(symbol_code)
        if op_type == OpType.REGISTER:
            self.register_controller.registers[self.register_controller.DR] = symbol_code
        else:
            self.memory[self.register_controller.AR] = symbol_code
        logging.debug("input: %s", repr(symbol))

    def latch_data_address(self, mem_sel: bool):
        if mem_sel:
            self.register_controller.AR = DATA_START + self.register_controller.DR
        else:
            self.register_controller.AR = self.register_controller.registers[self.register_controller.DR]

    def latch_instruction_address(self):
        self.register_controller.AR = INSTRUCTION_START - 1 + self.register_controller.DR

    def latch_data(self, operand_val, op_type: OpType):
        if op_type == OpType.INDIRECT and operand_val not in self.register_controller.registers:
            self.register_controller.DR = self.memory[self.register_controller.AR]
        else:
            self.register_controller.DR = operand_val

    def store_data(self, reg, reg_t: OpType, op_val, op_type: OpType):
        if op_type == OpType.REGISTER:
            value = (self.register_controller.registers
                     if reg_t == OpType.DIRECT else self.register_controller.registers)
        else:
            value = (op_val if op_type == OpType.IMMEDIATE else self.register_controller.AC)
        store = (self.memory if reg_t == OpType.DIRECT else self.register_controller.registers)
        store[reg] = value[op_val] if op_type == OpType.REGISTER else value

    def latch_acc(self, mem_sel: OpType):
        if mem_sel == OpType.DIRECT or mem_sel == OpType.INDIRECT:
            self.register_controller.AC = self.memory[self.register_controller.AR]
        else:
            self.register_controller.AC = self.register_controller.DR

    def latch_alu(self, sel_left: bool, mem_sel: OpType, reg: str):
        mem_map = {
            OpType.DIRECT or OpType.INDIRECT: lambda: self.memory[self.register_controller.AR],
            OpType.REGISTER: lambda: self.register_controller.registers[reg],
            OpType.IMMEDIATE: lambda: self.register_controller.DR,
        }
        if sel_left:
            self.alu.left = mem_map.get(mem_sel)()
        else:
            self.alu.right = mem_map.get(mem_sel)()

    def latch_program_counter(self, sel_next: bool):
        if sel_next:
            self.register_controller.PC += 1
        else:
            self.register_controller.PC = self.register_controller.AR

    def set_flags(self, result: int):
        self.alu.flags["N"] = result < 0
        self.alu.flags["Z"] = result == 0
        self.alu.flags["V"] = (self.alu.left > 0 and self.alu.right > 0 and result < 0) | \
                              (self.alu.left < 0 and self.alu.right < 0 and result > 0)
        self.alu.flags["C"] = result > (2 ** 32 - 1) | result < -(2 ** 32)

    def execute_alu(self, opcode: Opcode):
        res = self.alu.operations[Opcode(opcode)](self.alu.left, self.alu.right)
        while res > 2 ** 32 - 1:
            res = -(2 ** 32) + (res - (2 ** 32 - 1))
        while res < -(2 ** 32):
            res = (2 ** 32 - 1) - (res + 2 ** 32)
        self.register_controller.AC = res
        self.alu.right = 0
        self.alu.left = 0

    def latch_register(self, opcode: Opcode, register: str, is_there_2_ops: bool, is_the_same_instruction: bool):
        if not is_the_same_instruction and not is_there_2_ops and opcode not in {Opcode.DIV, Opcode.MOD, Opcode.MUL}:
            self.register_controller.registers[register] += self.register_controller.AC
        else:
            self.register_controller.registers[register] = self.register_controller.AC
        self.set_flags(self.register_controller.registers[register])

    def shift(self, opcode: Opcode) -> bool:
        should_jump = ((opcode == Opcode.JG and not (self.alu.flags["N"] or self.alu.flags["Z"]))
                       or (opcode == Opcode.JNZ and not self.alu.flags["Z"])
                       or (opcode == Opcode.JMP))
        return should_jump


class ControlUnit:
    def __init__(self, data_path, register_controller):
        self.data_path: DataPath = data_path
        self.register_controller: RegisterController = register_controller
        self._tick: int = 0

    def tick(self):
        self._tick += 1

    def current_tick(self):
        return self._tick

    def set_alu_val(self, is_left: bool, op_type: OpType, reg):
        if op_type == OpType.REGISTER:
            self.data_path.latch_alu(not is_left, op_type, reg)
            self.tick()
        elif op_type == OpType.DIRECT:
            self.data_path.latch_data_address(True)
            self.tick()
        elif op_type == OpType.INDIRECT:
            if self.register_controller.DR in self.register_controller.registers:
                self.data_path.latch_data_address(False)
                self.tick()
                self.data_path.latch_acc(op_type)
                self.tick()
                self.register_controller.registers[reg] = self.register_controller.AC
            else:
                self.data_path.latch_data_address(True)
                self.tick()
                self.data_path.latch_data(None, op_type)
                self.tick()
                self.data_path.latch_data_address(True)
                self.tick()
        if is_left:
            self.data_path.latch_alu(False, op_type, reg)
            self.tick()
        elif not is_left:
            self.data_path.latch_acc(op_type)
            self.tick()
            self.data_path.latch_alu(True, op_type, reg)
            self.tick()

    def decode_and_execute_instruction(self, register_controller: RegisterController, alu: Alu,
                                       is_the_same_instruction: bool):
        opcode = register_controller.CIR["opcode"]
        term = register_controller.CIR["term"]
        if opcode == Opcode.HLT:
            raise StopIteration()
        elif opcode in {Opcode.JG, Opcode.JNZ, Opcode.JMP}:
            should_jump = ((opcode == Opcode.JG and not (alu.flags["N"] or alu.flags["Z"]))
                           or (opcode == Opcode.JNZ and not alu.flags["Z"])
                           or (opcode == Opcode.JMP))
            if should_jump:
                self.data_path.latch_data(term[1], None)
                self.tick()
                self.data_path.latch_instruction_address()
                self.data_path.latch_program_counter(False)
            self.data_path.latch_program_counter(not should_jump)
            self.tick()
        elif opcode == Opcode.IN:
            operand_value, op_type = term[1:]
            if op_type == OpType.REGISTER.value:
                self.data_path.latch_data(operand_value, op_type)
            else:
                self.data_path.latch_data_address(True)
            self.tick()
            self.data_path.wr(op_type)
            self.tick()
            self.data_path.latch_program_counter(True)
        elif opcode == Opcode.OUT:
            operand_value, operand_type = term[1:]
            self.data_path.latch_data(operand_value, operand_type)
            self.tick()
            if operand_type == OpType.DIRECT:
                self.data_path.latch_data_address(True)
                self.tick()
            self.data_path.output()
            self.data_path.latch_program_counter(True)
        elif opcode == Opcode.MOV:
            reg, reg_t, op_val, op_type = term[1:]
            self.data_path.latch_program_counter(True)
            self.data_path.latch_data(op_val, op_type)
            self.tick()
            self.data_path.latch_data_address(op_type == OpType.DIRECT)
            self.tick()
            self.data_path.latch_acc(op_type)
            self.tick()
            self.data_path.store_data(reg, reg_t, op_val, op_type)
            self.tick()
        elif opcode == Opcode.LEA:
            register_sel, operand_type, operand_value2, operand_type2 = term[1:]
            self.data_path.latch_data(operand_value2, operand_type2)
            self.tick()
            self.data_path.latch_acc(operand_type)
            self.tick()
            self.data_path.latch_register(opcode, register_sel, False, False)
            self.tick()
            self.data_path.latch_program_counter(True)
        else:
            register_sel, operand_type, operand_value, operand_type2, *other = term[1:]
            operand_value3, operand_type3 = other if len(other) == 2 else (None, None)
            if opcode == Opcode.CMP:
                operand_value, operand_type2, operand_value3, operand_type3 = term[1:]
            is_one_operand = operand_value3 is None
            self.data_path.latch_program_counter(True)
            self.data_path.latch_data(operand_value, operand_type2)
            self.tick()
            if is_one_operand:
                if opcode in {Opcode.DIV, Opcode.MOD, Opcode.MUL}:
                    self.set_alu_val(True, operand_type, register_sel)
                self.set_alu_val(False, operand_type2, (register_sel, operand_value)[operand_type2 == OpType.REGISTER])
            else:
                is_reg2, is_reg3 = operand_type2 == OpType.REGISTER, operand_type3 == OpType.REGISTER
                self.set_alu_val(True, operand_type2, (operand_value, register_sel)[is_reg2])
                self.data_path.latch_data(operand_value3, operand_type3)
                self.tick()
                self.set_alu_val(False, operand_type3, (register_sel, operand_value3)[is_reg3])
            self.data_path.execute_alu(opcode)
            self.tick()
            self.data_path.set_flags(register_controller.AC)
            if opcode != Opcode.CMP:
                if not is_the_same_instruction or (is_one_operand and opcode in {Opcode.DIV, Opcode.MOD, Opcode.MUL}):
                    self.data_path.latch_register(opcode, register_sel, not is_one_operand, is_the_same_instruction)
                else:
                    self.data_path.latch_data(register_sel, OpType.REGISTER)
                    self.tick()
                    self.set_alu_val(True, OpType.REGISTER, register_sel)
                    self.data_path.latch_data(self.register_controller.AC, OpType.IMMEDIATE)
                    self.tick()
                    self.set_alu_val(False, OpType.IMMEDIATE, register_sel)
                    self.data_path.execute_alu(opcode)
                    self.tick()
                    self.data_path.latch_register(opcode, register_sel, not is_one_operand, is_the_same_instruction)
                self.tick()


    def __repr__(self):
        opcode = self.register_controller.CIR["opcode"]
        term = self.register_controller.CIR["term"]
        state = "TICK: {}, PC: {}, AR: {}, OUT: {}, ACC: {},\nREGS:{}".format(
            self._tick,
            self.register_controller.PC,
            self.register_controller.AR,
            (self.data_path.output_buffer[-1]) if opcode == Opcode.OUT else 0,
            self.register_controller.AC,
            self.register_controller.registers,
        )

        format_strings = {
            TermTwoOperands: "{} DESTINATION:[{}-{}], OPERAND1:[{}-{}], OPERAND2:[{}-{}]",
            TermDestination: "{} [{}-{}]",
            TermOneOperand: "{} DESTINATION:[{}-{}], OPERAND1:[{}-{}]"
        }

        if isinstance(term, TermTwoOperands):
            format_string = format_strings[TermTwoOperands]
            args = [term.destination, term.mod, term.operand, term.op_type, term.operand2, term.op_type2]
        elif isinstance(term, TermDestination):
            format_string = format_strings[TermDestination]
            args = [term.operand, term.mod]
        elif isinstance(term, TermOneOperand):
            format_string = format_strings[TermOneOperand]
            args = [term.destination, term.mod, term.operand, term.op_type]
        else:
            format_string = "{}"
            args = []

        action = format_string.format(Opcode(opcode).name, *args)

        return "{}\n{}".format(action, state)


def simulation(code, input_buffer, limit):
    instr_counter = 0
    memory = Memory(MEM_SIZE, INSTRUCTION_START, DATA_START)
    memory.load("data", code["data"].values())
    memory.load("instructions", code["text"])
    register_controller = RegisterController()
    alu = Alu()
    datapath = DataPath(register_controller, memory, alu, input_buffer=input_buffer)
    control_unit = ControlUnit(datapath, register_controller)
    curr2 = None
    try:
        while True:
            assert limit > instr_counter, "too long execution, increase limit!"
            register_controller.fetch_instruction(memory.memory)
            term_0 = register_controller.CIR["term"][0]
            is_the_same = term_0 == curr2
            control_unit.decode_and_execute_instruction(register_controller, alu, is_the_same)
            curr2 = register_controller.CIR["term"][0]
            logging.debug("%s", control_unit)
            instr_counter += (not is_the_same)
    except EOFError:
        logging.warning("Input buffer is empty!")
    except StopIteration:
        pass
    logging.info("output_buffer: %s", repr("".join(datapath.output_buffer)))
    return "".join(datapath.output_buffer), instr_counter, control_unit.current_tick()


def main(args):
    code_file, input_file = args
    input_token = []
    code = read_code(code_file)
    with open(input_file, encoding="utf-8") as file:
        input_text = file.read()
        for char in input_text:
            input_token.append(char)
    output, instr_counter, ticks = simulation(code, input_token, limit=1000)

    print("".join(output))
    print("instr_counter:", instr_counter, "ticks:", ticks)


if __name__ == "__main__":
    logging.getLogger().setLevel(logging.DEBUG)
    main(sys.argv[1:])
