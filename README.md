# AKlab3

Вариант:
`asm | cisc | neum | hw | instr | struct | stream | port | prob2`

Студент: 
Жолеу Алтынай 

# Язык программирования

``` ebnf
program ::= line "\n" program

line ::= label
       | command
       | section

label ::= name ":"

command ::= "\t" operation_n_args " " destination ", " operand ", ... " operand "|
            "\t" operation_2_args " " destination ", " operand |
            "\t" operation_1_arg " " operand |
            "\t" operation_0_args |

section ::= "section ." section_name

section_name ::= "text" | "data"

operation_n_args ::= "add" | "sub" | "mul" | "mod" | "div" , where n >= 2

operation_2_args ::= "cmp" | "mov" | "lea" 

operation_1_arg ::= "jnz" | "jg" | "jmp" | "in" | "out" 

operation_0_args ::= "hlt"

operand ::= register | name | number    ; "name" here is label name

name ::= [a-z_]+
          
number ::= [-2^32; 2^32 - 1]

register ::= [RAX, RBX, RCX, RDX]
```

Код выполняется последовательно. Операции:

- `add <destination> <args>` -- прибавить к первому аргументу последующие аргументы
- `sub <destination> <args>` -- вычесть из первого аргумента последующие аргументы
- `mov <arg1> <arg2>` -- скопировать значение из второго аргумента в первый
- `div <destination> <args>` -- получить целую часть от деления первого аргумента на последующие аргументы
- `mod <destination> <args>` -- получить остаток от деления первого аргумента на последующие аргументы
- `mul <destination> <args>` -- получить произведение первого аргумента на последующие аргументы
- `cmp <arg1> <arg2>` -- получить результат сравнения первого аргумента со вторым (0, если аргументы равны)
- `jg <label>` -- если значение в аккумуляторе больше нуля, перейти на аргумент-метку
- `jnz <label>` -- если значение в аккумуляторе отлично от нуля, перейти на аргумент-метку
- `jmp <label>` -- безусловный переход на аргумент-метку
- `out <arg>` -- распечатать в поток вывода значение из аргумента
- `in <arg>` -- прочитать в аргумент значение из потока ввода
- `hlt` -- завершить выполнение программы

Поддерживаемые аргументы:
- регистры `RAX`,`RBX`, `RCX`, `RDX`
- название **объявленной** метки
- число в диапазоне [-2^32; 2^32 - 1]

Дополнительные конструкции:
- `.text` - объявление секции кода
- `.data` - объявление секции данных
- `<label>:` - метки для переходов / названия переменных

*Примечания:
- `ADD %RAX,#5` - RAX <- RAX + 5 , прибавит к значению в регистре 5
- `ADD %RBX,#5,#9` - RBX <- 5 + 9 , сложит 5 и 9 и запишет резултат в регистр


## Организация памяти

Память данных и команд совмещены. Начальный адрес данных, команд и размер памяти задаётся в глобальных переменных в `machine.py`.

### Набор инструкции

| Syntax                        | Ticks | Comment                                         |
|:------------------------------|:------|:------------------------------------------------|
| `add/sub <dest> <args>`       | 9-24  | кол-во тактов зависит от операндов и типа адресации             |
| `mov <dest> <arg>`            |  4    | см. язык               |
| `cmp <arg> <arg>`             | 9-12  | кол-во тактов зависит от адресации              |
| `jg/jnz/jmp <arg>`            | 1-2   | кол-во тактов зависит от перехода                                        |
| `hlt`                         | 0     | см. язык                                        |
| `out <arg>`                   | 2   | запись в стандартный поток вывода из регистра(числа)      |
| `in <arg>`                    | 2   | запись в регистр из устройства ввода|


### Кодирование инструкций

* Машинный код сериализуется в список JSON.
* Если инструкция не связана с операциями алу, то один элемент списка, одна инструкция.
* Индекс списка -- адрес инструкции. Используется для проверки длинной инструкции, которая делится на последовательные подинструкции.

Пример:

```json
{
  "opcode": "mov",
   "term": [
      1,
      "RAX",
      "register",
      11,
      "direct"
    ]
},
```
где:
* 'opcode' -- код операции
* 'term' -- слово
* первый элемент -- индекс инструкции
* второй элемент -- куда сохраняем
* второй элемент -- адресация 
* второй элемент -- откуда сохраняем
* второй элемент -- адресация

Пример длинной инструкции:
```
.data
  NUM:1
  NEW_NUM:2
.text
  ADD %RCX,NUM,#10,NEW_NUM,#20,%RAX
```
```json
  {
  "opcode": "add",
   "term": [
                  1,
                  "RCX",
                  "register",
                  0,
                  "direct",
                  10,
                  "immediate"
              ]
  },
  {
  "opcode": "add",
  "term": [
                  1,
                  "RCX",
                  "register",
                  1,
                  "direct",
                  20,
                  "immediate"
              ]
   },
   {
     "opcode": "add",
      "term": [
                  1,
                  "RCX",
                  "register",
                  "RAX",
                  "register"
              ]
    }
```
Типы данные в модуле [isa](./isa.py), где:

* `Opcode` -- перечисление кодов операций;
* `OpType` -- тип адресации операнда
* `Term` -- структура для описания значимого фрагмента кода исходной программы.

## Транслятор

Интерфейс командной строки: `translator.py <input_file.asm> <target_file.json>"`

Реализовано в модуле: [translator](translation/translator.py)

Пример кода hello:

```
  .data
  WORD:"hello world"
  WORD_COUNT:11
  .text
  START:
    MOV %RAX,WORD_COUNT
    LEA %RDX,WORD
  LOOP:
    MOV %RBX,[%RDX]
    OUT %RBX
    ADD %RDX,#1
    SUB %RAX,#1
    JG LOOP
  END:
    HLT
```
Результат трансляции файл в json

```
{
      "data": {
          "WORD": [
              104,
              101,
              108,
              108,
              111,
              32,
              119,
              111,
              114,
              108,
              100
          ],
          "WORD_COUNT": 11
      },
      "text": [
          {
              "opcode": "mov",
              "term": [
                  1,
                  "RAX",
                  "register",
                  11,
                  "direct"
              ]
          },
          {
              "opcode": "lea",
              "term": [
                  2,
                  "RDX",
                  "register",
                  0,
                  "direct"
              ]
          },
          {
              "opcode": "mov",
              "term": [
                  3,
                  "RBX",
                  "register",
                  "RDX",
                  "indirect"
              ]
          },
          {
              "opcode": "out",
              "term": [
                  4,
                  "RBX",
                  "register"
              ]
          },
          {
              "opcode": "add",
              "term": [
                  5,
                  "RDX",
                  "register",
                  1,
                  "immediate"
              ]
          },
          {
              "opcode": "sub",
              "term": [
                  6,
                  "RAX",
                  "register",
                  1,
                  "immediate"
              ]
          },
          {
              "opcode": "jg",
              "term": [
                  7,
                  3,
                  "direct"
              ]
          },
          {
              "opcode": "hlt",
              "term": [
                  8
              ]
          }
      ]
  }

```
## Модель процессора

Реализовано в модуле: [machine](machine.py).

[image](https://gitlab.se.ifmo.ru/zholeu/aklab3/-/blob/master/Schema.jpg)

Регистры:

- `PC` -- для указания на текущую выполняющуюся команду
- `AR` -- для указания адреса, по которому осуществляется вызов к памяти
- `AC` -- аккумулятор
- `DR` -- для данных полученных из памяти
- `CIR` -- инструкция которая выполняется в данный момент
- `RAX, RBX, RCX, RDX` -- для манипуляции данными

Особенности работы модели:

- Для журнала состояний процессора используется стандартный модуль logging.
- Количество инструкций для моделирования ограничено hardcoded константой.
- Остановка моделирования осуществляется при помощи исключений:
    - `StopIteration` -- если выполнена инструкция `halt`.
- Управление симуляцией реализовано в файле `simulator`.


## Апробация

В качестве тестов использовано 4 алгоритма:

1. [hello](examples/hello.asm).
2. [cat](examples/cat.asm) -- программа `cat`, повторяем ввод на выводе.
3. [prob2](examples/prob2.asm).
4. [long_instruction](examples/long_instruction.asm) -- проверка CISC.

Интеграционные тесты реализованы тут: [integration_test](integration_test.py)

CL:
```yaml
lab3-example:
  stage: test
  image:
    name: python-tools
    entrypoint: [""]
  script:
    - pip install pytest-golden
    - true && python3 -m pytest . -vvv --update-goldens
    - python3-coverage run -m pytest --verbose
    - find . -type f -name "*.py" | xargs -t python3-coverage report
    - find . -type f -name "*.py" | xargs -t pep8 --ignore=E501,W503,W293,E303
    - find . -type f -name "*.py" | xargs -t pylint
```
где:

- `python3-coverage` -- формирование отчёта об уровне покрытия исходного кода.
- `pytest` -- утилита для запуска тестов.
- `pep8` -- утилита для проверки форматирования кода. `E501` (длина строк) отключено.
- `pylint` -- утилита для проверки качества кода. Некоторые правила отключены в отдельных модулях с целью упрощения кода.
- `mypy` -- утилита для проверки корректности статической типизации.
  - `--check-untyped-defs` -- дополнительная проверка.
  - `--explicit-package-bases` и `--namespace-packages` -- помогает правильно искать импортированные модули.
- Docker image `python-tools` включает в себя все перечисленные утилиты.

Пример использования и журнал работы процессора на примере `cat`:

``` commandline
$ cat input.txt
foo

$ py ./translation/translator.py examples/cat.asm cat.json

$ cat cat.json
{
    "data": {},
    "text": [
        {
            "opcode": "in",
            "term": [
                1,
                "RBX",
                "register"
            ]
        },
        {
            "opcode": "out",
            "term": [
                2,
                "RBX",
                "register"
            ]
        },
        {
            "opcode": "jmp",
            "term": [
                3,
                1,
                "direct"
            ]
        }
    ]
}
$ py machine.py cat.json input.txt
DEBUG:root:input: 'f'
DEBUG:root:IN [RBX-register]
TICK: 2, PC: 21, AR: 20, OUT: 0, ACC: 0,
REGS:{'RAX': 0, 'RBX': 102, 'RCX': 0, 'RDX': 0}
DEBUG:root:output: '' << 'f'
DEBUG:root:OUT [RBX-register]
TICK: 3, PC: 22, AR: 21, OUT: 102, ACC: 0,
REGS:{'RAX': 0, 'RBX': 102, 'RCX': 0, 'RDX': 0}
DEBUG:root:JMP [1-direct]
TICK: 5, PC: 20, AR: 20, OUT: 0, ACC: 0,
REGS:{'RAX': 0, 'RBX': 102, 'RCX': 0, 'RDX': 0}
DEBUG:root:input: 'o'
DEBUG:root:IN [RBX-register]
TICK: 7, PC: 21, AR: 20, OUT: 0, ACC: 0,
REGS:{'RAX': 0, 'RBX': 111, 'RCX': 0, 'RDX': 0}
DEBUG:root:output: 'f' << 'o'
DEBUG:root:OUT [RBX-register]
TICK: 8, PC: 22, AR: 21, OUT: 111, ACC: 0,
REGS:{'RAX': 0, 'RBX': 111, 'RCX': 0, 'RDX': 0}
DEBUG:root:JMP [1-direct]
TICK: 10, PC: 20, AR: 20, OUT: 0, ACC: 0,
REGS:{'RAX': 0, 'RBX': 111, 'RCX': 0, 'RDX': 0}
DEBUG:root:input: 'o'
DEBUG:root:IN [RBX-register]
TICK: 12, PC: 21, AR: 20, OUT: 0, ACC: 0,
REGS:{'RAX': 0, 'RBX': 111, 'RCX': 0, 'RDX': 0}
DEBUG:root:output: 'fo' << 'o'
DEBUG:root:OUT [RBX-register]
TICK: 13, PC: 22, AR: 21, OUT: 111, ACC: 0,
REGS:{'RAX': 0, 'RBX': 111, 'RCX': 0, 'RDX': 0}
DEBUG:root:JMP [1-direct]
TICK: 15, PC: 20, AR: 20, OUT: 0, ACC: 0,
REGS:{'RAX': 0, 'RBX': 111, 'RCX': 0, 'RDX': 0}
DEBUG:root:input: '\n'
DEBUG:root:IN [RBX-register]
TICK: 17, PC: 21, AR: 20, OUT: 0, ACC: 0,
REGS:{'RAX': 0, 'RBX': 10, 'RCX': 0, 'RDX': 0}
DEBUG:root:output: 'foo' << '\n'
DEBUG:root:OUT [RBX-register]
TICK: 18, PC: 22, AR: 21, OUT: 10, ACC: 0,
REGS:{'RAX': 0, 'RBX': 10, 'RCX': 0, 'RDX': 0}
DEBUG:root:JMP [1-direct]
TICK: 20, PC: 20, AR: 20, OUT: 0, ACC: 0,
REGS:{'RAX': 0, 'RBX': 10, 'RCX': 0, 'RDX': 0}
WARNING:root:Input buffer is empty!
INFO:root:output_buffer: 'foo\n'
foo

instr_counter: 12 ticks: 21
```

| ФИО             | алг.  | LoC | code байт | code инстр. | инстр. | такт. | вариант |
|-----------------|-------|-----|-----------|-------------|--------|-------|---------|
| Жолеу А.        | hello | 15  | -         | 8           | 57     | 193   | -       |
| Жолеу А.        | cat   | 5   | -         | 3           | 12     | 21    | -       |
| Жолеу А.        | prob2 | 26  | -         | 15          | 80     | 382   | -       |





