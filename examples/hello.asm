.data
WORD:"hello world"
WORD_COUNT:11
.text
    START:
        MOV %RAX,WORD_COUNT
        LEA %RDX,WORD
    LOOP:
        MOV %RBX,[%RDX]
        OUT %RBX
        ADD %RDX,#1
        SUB %RAX,#1
        JG LOOP
    END:
        HLT
