.data
NUM:1000
NEW_NUM:2000
SPACE:" "
.text
    START:
        MOV %RAX,NUM
        ADD %RAX,NEW_NUM
        OUT %RAX
        MOV %RDX,SPACE
        OUT %RDX
        MOV %RBX,NUM
        ADD %RBX,NEW_NUM,#40
        OUT %RBX
        OUT %RDX
        ADD %RCX,NUM,#10,NEW_NUM,#20,#30,%RBX,#100
        OUT %RCX
    END:
        HLT
