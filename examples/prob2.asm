.data
limit:4000000
a:2
b:8
c:34
sum_a_and_b:10
RESULT:0
.text
    START:
        MOV %RDX,sum_a_and_b
        MOV %RAX,a
        MOV %RBX,b
        MOV %RCX,c
    fibonacci_loop:
        CMP %RCX,limit
        JG end_fibonacci_loop
        ADD %RDX,%RCX
        MOV %RAX,%RBX
        MOV %RBX,%RCX
        MUL %RCX,#4,%RBX
        ADD %RCX,%RAX
        JMP fibonacci_loop
    end_fibonacci_loop:
        MOV RESULT,%RDX
        OUT %RDX
        HLT
