# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring
# pylint: disable=line-too-long
# pylint: disable=trailing-whitespace
# pylint: disable=useless-import-alias
# pylint: disable=missing-module-docstring
import contextlib
import io
import logging
import os
import tempfile
import unittest
import pytest as pytest
import translator
import machine


@pytest.mark.golden_test("examples/*.yml")
def test_whole_by_golden(golden, caplog):
    caplog.set_level(logging.DEBUG)

    with tempfile.TemporaryDirectory() as tmpdirname:
        source_file = os.path.join(tmpdirname, "example.asm")
        target = os.path.join(tmpdirname, "example.json")
        input_file = os.path.join(tmpdirname, "input.txt")

        with open(source_file, "w", encoding="utf-8") as file:
            file.write(golden["source"])
        with open(input_file, "w", encoding="utf-8") as file:
            file.write(golden["input"])

        with contextlib.redirect_stdout(io.StringIO()) as stdout:
            translator.main([source_file, target])
            machine.main([target, input_file])

        with open(target, encoding="utf-8") as file:
            code = file.read()

        assert code == golden.out["code"]
        assert stdout.getvalue() == golden.out["output"]
        assert caplog.text == golden.out["log"]



class TestCases(unittest.TestCase):

    def test_cat(self):
        with tempfile.TemporaryDirectory() as tmpdirname:
            source = "examples/cat.asm"
            target = os.path.join(tmpdirname, "cat.json")
            input_stream = "input.txt"
            with contextlib.redirect_stdout(io.StringIO()) as stdout:
                translator.main([source, target])
                machine.main([target, input_stream])
                self.assertEqual(stdout.getvalue(),
                                 'foo\n'
                                 '\ninstr_counter: 12 ticks: 21\n'
                                 )

    def test_hello(self):
        with tempfile.TemporaryDirectory() as tmpdirname:
            source = "examples/hello.asm"
            target = os.path.join(tmpdirname, "hello.json")
            input_stream = "input.txt"
            with contextlib.redirect_stdout(io.StringIO()) as stdout:
                translator.main([source, target])
                machine.main([target, input_stream])
            self.assertEqual(stdout.getvalue(),
                             'hello world\n'
                             'instr_counter: 57 ticks: 193\n'
                             )

    def test_prop2(self):
        with tempfile.TemporaryDirectory() as tmpdirname:
            source = "examples/prob2.asm"
            target = os.path.join(tmpdirname, "prob2.json")
            input_stream = "input.txt"

            with contextlib.redirect_stdout(io.StringIO()) as stdout:
                translator.main([source, target])
                machine.main([target, input_stream])
                self.assertEqual(stdout.getvalue(),
                                 '4613732\n'
                                 'instr_counter: 80 ticks: 382\n')
    
    def test_long_instruction(self):
        with tempfile.TemporaryDirectory() as tmpdirname:
            source = "examples/long_instruction.asm"
            target = os.path.join(tmpdirname, "instr.json")
            input_stream = "input.txt"

            with contextlib.redirect_stdout(io.StringIO()) as stdout:
                translator.main([source, target])
                machine.main([target, input_stream])
                self.assertEqual(stdout.getvalue(),
                                 '3000 2040 5200\n'
                                 'instr_counter: 11 ticks: 81\n')
