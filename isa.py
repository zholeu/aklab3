# pylint: disable=missing-class-docstring
# pylint: disable=too-few-public-methods
# pylint: disable=trailing-whitespace
# pylint: disable=missing-module-docstring
import json
from collections import namedtuple
from enum import Enum


class Opcode(str, Enum):
    ADD = 'add'
    SUB = 'sub'
    MUL = 'mul'
    CMP = 'cmp'
    MOD = 'mod'
    DIV = 'div'
    MOV = 'mov'
    JMP = 'jmp'
    HLT = 'hlt'
    JG = 'jg'
    JNZ = 'jnz'
    OUT = 'out'
    IN = 'in'
    LEA = 'lea'


class OpType(str, Enum):
    IMMEDIATE = 'immediate'
    REGISTER = 'register'
    DIRECT = 'direct'
    INDIRECT = 'indirect'


class TermZeroOperand(namedtuple('Term', 'line')):
    """Описание выражения из исходного текста программы."""
    
    
class TermOneOperand(namedtuple('Term', 'line destination mod operand op_type')):
    """Описание выражения из исходного текста программы."""
    
    
class TermTwoOperands(namedtuple('Term', 'line destination mod operand op_type operand2 op_type2')):
    """Описание выражения из исходного текста программы."""


class TermDestination(namedtuple('Term', 'line operand mod')):
    """Описание выражения из исходного текста программы."""


def write_code(filename, code):
    """Записать машинный код в файл."""
    with open(filename, "w", encoding="utf-8") as file:
        file.write(json.dumps(code, indent=4))


def read_code(filename):
    """Прочесть машинный код из файла."""
    with open(filename, encoding="utf-8") as file:
        code = json.loads(file.read())

    for instr in code['text']:
        instr['opcode'] = Opcode(instr['opcode'])
        term_args = instr['term']
        term_len = len(term_args)
        if instr['opcode'] == Opcode.HLT:
            instr['term'] = TermZeroOperand(instr['term'][0])
        elif 'term' in instr:
            instr['term'] = TermOneOperand(*term_args) if term_len == 5 else (
                TermDestination(*term_args) if term_len == 3 else TermTwoOperands(*term_args))
    return code
