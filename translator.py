# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring
# pylint: disable=missing-module-docstring
# pylint: disable=line-too-long
# pylint: disable=too-many-instance-attributes
# pylint: disable=no-else-return
# pylint: disable=unused-variable
# pylint: disable=too-many-return-statements
# pylint: disable=too-many-branches
# pylint: disable=too-many-locals
# pylint: disable=too-many-nested-blocks
# pylint: disable=redefined-builtin
# pylint: disable=too-many-statements
# pylint: disable=unspecified-encoding

import json
import re
import sys

from isa import Opcode


class AssemblerTranslator:
    def __init__(self):
        self.in_text_section = False
        self.in_text_section1 = False
        self.in_data_section = False
        self.data_section = {}
        self.text_section = []
        self.labels = []
        self.label_k = []
        self.label_c = []
        self.current_label = None
        self.current_label1 = None
        self.current_label2 = None
        self.prev_current_label2 = None
        self.counter = 1
        self.index = 1

    def translate_data_section(self, line):
        if self.in_data_section and ':' in line:
            label, value = line.split(':')
            value = value.strip()

            if value.startswith('"') and value.endswith('"'):
                value = [ord(c) for c in value[1:-1]]
                self.data_section.setdefault(label, []).extend(value)
            else:
                self.data_section[label.strip()] = int(value.strip())


    def get_operand_info(self, operand):
        str_a = ""
        if operand.startswith('#'):
            return (int(operand.strip().split('#')[1]), "immediate")
        elif operand.startswith('%'):
            return (operand.strip().split('%')[1], "register")
        elif operand.startswith('['):
            matches = re.findall(r'\[([^\]]+)\]', operand.strip())
            matches_without_percent = [match.replace('%', '') for match in matches]
            result_string = ''.join(matches_without_percent)

            if result_string in self.data_section:
                data_section_list = list(self.data_section.items())
                index = 0
                for i, (label, value) in enumerate(data_section_list):
                    if label == result_string:
                        break
                    index += len(value) if isinstance(value, list) else 1
                return (index, "indirect")
            else:
                return (result_string, "indirect")
        elif operand in self.data_section or operand.startswith('@'):
            if operand.startswith('@'):
                return (int(operand.strip().split('@')[1]), "direct")
            else:
                data_section_list = list(self.data_section.items())
                index = 0
                for i, (label, value) in enumerate(data_section_list):
                    if label == operand:
                        break
                    index += len(value) if isinstance(value, list) else 1
                self.data_section = dict(data_section_list)
                return (index, "direct")
        elif operand.strip().split(',')[0] is not str_a:
            value = next((item[operand.strip().split(',')[0]]
                          for item in self.label_c
                          if operand.strip().split(',')[0] in item), None)

            return value, "direct"

        else:
            return (None, None)

    def translate_text_section2(self, line):
        op_code = None
        if line != '':
            op_code = line.strip().split()[0].lower()

        if ':' in line:
            self.current_label2 = line.split(':')[0]
            if self.current_label2 != self.prev_current_label2:
                self.label_c.append({self.current_label2.strip(): self.index})

                self.prev_current_label2 = self.current_label2

        terms = line.split(',')
        if ',' in line or op_code in {Opcode.OUT, Opcode.IN, Opcode.HLT, Opcode.JMP, Opcode.JG, Opcode.JNZ, Opcode.MUL}:
            op_code = line.strip().split()[0]
            if op_code.upper() == 'HLT':

                self.index += 1

            elif op_code.upper() == 'JMP':

                self.index += 1
            elif op_code.upper() == 'JG':
                self.index += 1

            else:
                if len(terms) > 0 and (',' in line or op_code.upper() == 'JG'):
                    op_code = terms[0].strip().split()[0]
                    operand1, operand1_type = self.get_operand_info(terms[0].strip().split()[1])
                    operand2, operand2_type = None, None
                    operand3, operand3_type = None, None

                    operand1_index = 1
                    operand2_index = 2
                    operand3_index = 3
                    end = False

                    for term in terms[1:]:

                        if end is False:
                            if len(terms) >= operand1_index:
                                operand2, operand2_type = self.get_operand_info(terms[operand1_index])
                                operand1_index += 1

                            if len(terms) <= operand1_index:
                                operand3, operand3_type = None, None
                                end = True

                            if len(terms) > operand2_index:
                                if len(terms) >= operand3_index:
                                    operand3, operand3_type = self.get_operand_info(terms[operand2_index])
                                    operand2_index += 1
                                    operand3_index += 1
                                    end = True
                                operand1_index += 1
                                operand2_index += 1
                            if len(terms) <= operand2_index and len(terms) <= operand1_index:
                                end = True
                            opcode = Opcode[op_code.upper()]
                            self.index += 1


    def translate_text_section(self, line):
        op_code = None
        if line != '':
            op_code = line.strip().split()[0].lower()

        if ':' in line:
            self.current_label = line.split(':')[0]
        stra = ''
        terms = line.split(',')
        if ',' in line or op_code in {Opcode.OUT, Opcode.IN, Opcode.HLT, Opcode.JMP, Opcode.JG, Opcode.JNZ, Opcode.MUL}:
            op_code = line.strip().split()[0]
            if op_code.upper() == 'HLT':
                self.text_section.append({
                    "opcode": "hlt",
                    "term": [self.counter]
                })
                self.counter += 1

            elif op_code.upper() == 'JMP':
                operand, type = self.get_operand_info(terms[0].strip().split()[1])
                operand2, type = self.get_operand_info(terms[0].strip().split()[1])
                self.text_section.append({
                    "opcode": "jmp",
                    "term": [self.counter, operand, type]
                })
                self.counter += 1
            elif op_code.upper() == 'LEA':
                operand, type = self.get_operand_info(terms[0].strip().split()[1])
                operand2, type2 = self.get_operand_info(terms[1].strip())
                self.text_section.append({
                    "opcode": "lea",
                    "term": [self.counter, operand, type, operand2, type2]
                })
                self.counter += 1
            elif op_code.upper() == 'JG':
                operand, type = self.get_operand_info(terms[0].strip().split()[1])
                self.text_section.append({
                    "opcode": "jg",
                    "term": [self.counter, operand, type]
                })
                self.counter += 1
            elif op_code.upper() == 'OUT':
                operand, type = self.get_operand_info(terms[0].strip().split()[1])
                self.text_section.append({
                    "opcode": "out",
                    "term": [self.counter, operand, type]
                })
                self.counter += 1
            elif op_code.upper() == 'IN':
                operand, type = self.get_operand_info(terms[0].strip().split()[1])
                self.text_section.append({
                    "opcode": "in",
                    "term": [self.counter, operand, type]
                })
                self.counter += 1
            else:
                if len(terms) > 0 and ',' in line:
                    op_code = terms[0].strip().split()[0]

                    operand1, operand1_type = self.get_operand_info(terms[0].strip().split()[1])
                    operand2, operand2_type = None, None
                    operand3, operand3_type = None, None

                    operand1_index = 1
                    operand2_index = 2
                    operand3_index = 3
                    end = False

                    for term in terms[1:]:

                        if not end:

                            if len(terms) >= operand1_index:
                                operand2, operand2_type = self.get_operand_info(terms[operand1_index])
                                operand1_index += 1
                            if len(terms) <= operand1_index:
                                operand3, operand3_type = None, None
                                end = True
                            if len(terms) > operand2_index:
                                if len(terms) >= operand3_index:
                                    operand3, operand3_type = self.get_operand_info(terms[operand2_index])
                                    operand2_index += 1
                                    operand3_index += 1
                                operand1_index += 1
                                operand2_index += 1
                            if len(terms) <= operand2_index and len(terms) <= operand1_index:
                                end = True
                            opcode = Opcode[op_code.upper()]
                            if operand2 is not None and operand3 is not None:
                                self.text_section.append({
                                    "opcode": opcode.value,
                                    "term": [
                                        self.counter,
                                        operand1,
                                        operand1_type,
                                        operand2,
                                        operand2_type,
                                        operand3,
                                        operand3_type,
                                    ]
                                })
                            elif operand3 is None:
                                self.text_section.append({
                                    "opcode": opcode.value,
                                    "term": [
                                        self.counter,
                                        operand1,
                                        operand1_type,
                                        operand2,
                                        operand2_type
                                    ]
                                })

                    self.counter += 1

    def translate(self, _assembly_code):
        lines = _assembly_code.split('\n')
        lines1 = _assembly_code.split('\n')
        first = False
        current_line = 0
        total_line_numbers = 1
        labels_count = 0
        for line1 in lines1:
            if line1.startswith('.text'):
                self.in_text_section1 = True
                continue
            if self.in_text_section1:
                current_line += 1
                if ':' in line1:
                    self.current_label1 = line1.split(':')[0]
                    labels_count += 1
                    self.labels.append({self.current_label1.strip(): current_line})

                    first = True

                if first and ':' not in line1:
                    self.label_k.append({self.current_label1.strip(): total_line_numbers - labels_count})

                total_line_numbers += 1

        for line in lines:
            if line.startswith('.data'):
                self.in_data_section = True
                continue
            if line.startswith('.text'):
                self.in_data_section = False
                self.in_text_section = True
                continue
            if self.in_text_section:
                self.translate_text_section2(line)

        for line in lines:
            if line.startswith('.data'):
                self.in_data_section = True
                continue
            if line.startswith('.text'):
                self.in_data_section = False
                self.in_text_section = True
                continue
            if self.in_data_section:
                self.translate_data_section(line)
            elif self.in_text_section:
                self.translate_text_section(line)

        return json.dumps({'data': self.data_section, 'text': self.text_section}, indent=4)


def main(args):
    src, machine = args
    with open(src, "r") as input_file:
        assembly_code = input_file.read()
    translator = AssemblerTranslator()
    json_output = translator.translate(assembly_code)

    with open(machine, "w") as file:
        file.write(json_output)


if __name__ == "__main__":
    main(sys.argv[1:])
